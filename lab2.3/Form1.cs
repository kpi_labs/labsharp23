﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab2._3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_MouseClick(object sender, MouseEventArgs e)
        {
            SaveText();
        }

        private void button2_MouseClick(object sender, MouseEventArgs e)
        {
            LoadText();
        }

        private void SaveText()
        {
            File.WriteAllText("textEditor.txt", textBox1.Text);
        }
        
        private void LoadText()
        {
            try
            {
                textBox1.Text = File.ReadAllText("textEditor.txt");
            }
            catch (FileNotFoundException ignore) { }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            LoadText();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveText();
        }
    }
}